# [ENG101x Portfolio](https://reactjs.org/)

Seeded from [gatsby-starter-blog](https://github.com/gatsbyjs/gatsby-starter-blog)

[![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg)](https://github.com/thiagodebastos/gatsby-blog/blob/master/LICENSE) [![npm version](https://img.shields.io/npm/v/gatsby.svg?style=flat)](https://www.npmjs.com/package/react) [![Node](https://img.shields.io/node/v/gatsby.svg)]() [![Build Status](https://travis-ci.org/thiagodebastos/gatsby-blog.svg?branch=master)](https://travis-ci.org/thiagodebastos/gatsby-blog)

---

## This blog is my submission for [ENG101x English Composition](https://www.edx.org/course/english-composition-asux-eng101x-6)

Contents comprise of:

- Home
- About this Site
- About Me
- WP#1
- WP#2
- WP#3
- Writer's Journals
